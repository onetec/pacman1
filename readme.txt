ERASMUS+ EUROLEAGUE CRAWLER PROJECT/LOS POLLOS HERMANOS GROUP
=======================================================
Documentation: https://drive.google.com/file/d/1mr56tFa3tPAPIKEPL76F9HpdBNWZkDTl/view

 ✸ INTRODUCTION
-----

The aim of this project is to create a "crawler" which can retrieve information from all players taking part in the European basquetball competition called "Euroleague".
The information retrieved would be the stats from each player during the "Euroleague" 2019-20 apart from other competitions each player has taken part in.

Afterwards, this information would be stored in dataframes. Each dataframe would be stored in a MongoDB database.
The information then would be used to create graphs according to specific questions.

     ➤ The following link will give access to all the code used in the making of this project --> https://bitbucket.org/onetec/pacman1/src/master/
     ➤ The following link will give access to the file which has the complete code you must run for this project --> https://bitbucket.org/onetec/pacman1/src/master/All%20tasks%20merged.ipynb

The project has been coded in Python and a couple of it's modules, which are Selenium and Matplotib.


 ✸ REQUIREMENTS
-----

This code can work in all Operative Systems. 
In order to be able to run all the code, you will need to have the following software fully working in your PC:

     ➤ Jupyter Notebook (This software will let you run the code and present all the data on screen) (Most recommended)

     ➤ PyCharm (In case you don't have Jupyter Notebook, this software will work as well, but output will be less clear and efficient)

     ➤ Python3 (Code language)

     ➤ Chromedriver (Which will let the code access the euroleague website and retrieve the information) 
		    (We used Google Chrome as the Web Browser to retrieve the information, making it the best browser to run this code on, but Firefox can also be used as well)

     ➤ MongoDB (You will need to MANUALLY create the database so that all the information can be stored into it)

     ➤ Selenium (This library will let the code access the "Euroleague" websites and extract the information from them)

     ➤ Pandas (This library will help at retrieving the information from MongoDB)

     ➤ Matplotib (This library will allow the code to create graphs with the information from the database)



 ✸ INSTALLATION
-----

     ➤ Python3:
	- Windows: You will need to download the installer from the following page --> https://www.python.org/downloads/windows/

	- Linux: It's really possible that you have Python installed, in order to check, run this command in console -> python --version
		 If you don't have it, run the following commands --> sudo apt-get update 
								  --> sudo apt-get install python 3.6

	- MacOS: In order to install Python, it is recommended to install Homebrew --> /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
	         Once it's installed, then run the following code --> brew install python3

     ➤ MongoDB:
	- Windows, Linux and MacOS: Download the installer from the following page, choose the installer suitable for you Operative System --> https://www.mongodb.com/download-center/community
				    Furthermore, you can install Mongo Compass which will help you at visualize everything, choose the installer suitable for you Operative System --> https://www.mongodb.com/download-center/compass

     ➤ Chromedriver:
	- Windows, Linux and MacOS: Download the installer from the following page, choose the installer suitable for you Operative System --> https://chromedriver.chromium.org/

     ➤ PhyCharm:
	- Windows: You will need to download the installer from the following page --> https://www.jetbrains.com/es-es/pycharm/download/#section=windows

	- Linux: You will need to download the installer from the following page --> https://www.jetbrains.com/es-es/pycharm/download/#section=linux

	- MacOS: You will need to download the installer from the following page --> https://www.jetbrains.com/es-es/pycharm/download/#section=mac

    From now on, the following installations will be done from python pip, which can be installed in console with the following command --> python get-pip.py

     ➤ 	Jupyter Notebook: It can be used with Anaconda, however, if you want to install only Jupyter, you will need to use the following commands --> pip install jupyterlab
																		  --> pip install notebook
     ➤ Selenium: In order to install Selenium, you will need to run the following command --> pip install selenium

     ➤ Pandas: In order to install Pandas, you will need to run the following command --> pip install pandas

     ➤ Matplotib: In order to install Matplotib, you will need to run the following command --> pip install matplotib

     If you want to install Anaconda, then you can download the installer from the following page --> https://www.anaconda.com/products/individual#Downloads

     When using Anaconda, you must add 'conda' before running any commands, failure to do so will result in the commands not being ran successfully, and therefore, nothing will be changed in return.
	
 
 ✸ CONFIGURATION
-----

The following code has a little menu, depending on user input, but has no modifiable settings. There is no configuration. When running, the code will automatically go through all players
automatically, and, when finished retrieving all information, the browser window will be closed automatically.


 ✸ STEPS TO RUN EVERYTHING CORRECTLY
-----

     1. Download the repository using Sourcetree.

     2. Open Jupyter Notebook and access the folder where all the files from the repository are stored.

     3. Make sure you have Chromedriver installed and working successfully if you're going to use Google Chrome.
	If you're going to use Firefox, then change driver = webdriver.Chrome() to driver = webdriver.Firefox()

     4. Access the file called "All tasks merged".

     5. Create the database in MongoDB.

     6. Run the file called "All tasks merged". Doing so will open a browser window with the Euroleague website, and it will refresh itself and go through all players automatically.
	MAKE SURE YOU DON'T CLOSE THIS WINDOW NOR TURN OFF THE COMPUTER/SUSPEND IT. If you do, you will need to start all over again. This crawler will take a little bit of time, so
	patience will be needed.

     7. Once it's finished, then now you'll be able to see all the data in the Database, when using Mongo Compass, just refresh the program, and when using commands, just write --> use euroleaguebasquet

     8. Once all the data is retrieved, now it's time to take a look at the graphs, for that you will need to access the file called "CompletedGraphs" and run it.


 ✸ TROUBLESHOOTING
-----

     ➤ If the code doesn't run properly in Jupyter Notebook, please make sure that the file extension is .ipynb
     ➤ Make sure you have the latest Chromedriver version, failure to do so will result in a crash of the code, not allowing it to access the Euroleague website.
     ➤ Make sure the name of your MongoDB database is called "euroleaguebasquet".

    
 ✸ QnA
-----

     Q. I see the code in the link mentioned in the introduction, I copy and paste it into my Jupyter Notebook file but when I click run it doesn't work. Why is that?
     A. Because you need to download the file and run that file into Jupyter Notebook. Copying and pasting the code won't make it work.

     Q. In that case, how do I download the file?
     A. The most recommended way to do so is by using Sourcetree, which can be downloaded accessing this site --> https://www.sourcetreeapp.com/
	You will need to create a folder in which you'll store the files you pull from the bitbucket repository. Afterwards, you will need to add this repository into Sourcetree so that
	you can download the files. Once you have done this correctly, you will need to go to the bitbucket page --> https://bitbucket.org/onetec/pacman1/src/master/
	and click on clone. Then, all the files will be added to your folder. Afterwards you will need to open the "All tasks merged" file with Jupyter Notebook and execute it there.

     Q. How do I create the Database using Mongo Compass?
     A. You will need to open Mongo Compass, and then click where it says "Create Database". You will need to give a name to the database, which will be "euroleaguebasquet", and then
	you will need to give a name to the collection, which will be players.

     Q. How do I create the Database if I don't have Mongo Compass?
     A. You will need to use the following command --> use euroleaguebasquet , this command will create a Database with the name "euroleaguebasquet". 
	Afterwards you will need to use --> db.player.insert({}) , this command will create an empty collection called "player".
	Finally, use --> show dbs , this way you'll know if the database has been successfully created.

	
 ✸ MAINTAINERS
-----

Current maintainers:

     ➤ James Yong (Spain)  
     ➤ Sergio Medina (Spain) 
     ➤ Jan Dufek (Czech Republic) 
     ➤ Jiří Hulmák (Czech Republic)
     ➤ Filip Moberg (Sweden) 
     ➤ Lucas Dunderberg (Sweden)


 ✸ SUPERVISION
-----

This project has been supervised by the following schools and companies:

     ➤ I.E.S. El Lago (Madrid, Spain)
     ➤ Grupo Onetec (Madrid, Spain)

	